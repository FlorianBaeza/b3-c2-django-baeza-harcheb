from django.urls import path
from . import views

urlpatterns = [
    path('create', views.site_form, name='create-form'),
    path('edit/<int:site_id>', views.site_form, name='edit-form'),
    path('delete/<int:site_id>', views.delete_site, name='delete-button'),
    path('', views.index, name='index')
]
