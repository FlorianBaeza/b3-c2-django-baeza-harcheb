from django.db import models

class Site(models.Model):
    # le nom du site auquel seront associées vos informations de connexion
    name = models.CharField(max_length=60)

    # l'url du site en question
    url = models.CharField(max_length=255)

    # la description du site. Peut être utile en cas de doublon, pour différencier deux profils sur un même site par exemple
    description = models.CharField(max_length=255)

    # l'identifiant de connexion au site
    identifier = models.CharField(max_length=150)

    # le mot de passe de connexion au site
    password = models.CharField(max_length=255)