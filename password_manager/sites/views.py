from django.shortcuts import render, redirect, get_object_or_404
from .models import Site
from .forms import SiteForm


def index(request):
    sites = Site.objects.all()
    return render(request, 'sites/listeSites.html', { 'sites': sites })


def site_form(request, site_id=None):
    # si site_id passé en paramètres, récupère l'instance de Site correspondante
    site = get_object_or_404(Site, id=site_id) if site_id is not None else None

    if request.method == 'POST':
        form = SiteForm(request.POST, instance=site)
        if form.is_valid():
            form.save()
            return redirect('index')
    else:
        form = SiteForm(instance=site)
    return render(request, 'sites/formulaireSites.html', { 'form' : form, 'site' : site })


def delete_site(request, site_id):
    site = get_object_or_404(Site, id=site_id)
    site.delete()
    return redirect('index')