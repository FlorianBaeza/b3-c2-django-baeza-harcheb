# Projet Password Manager

**Collaborateurs (B3 - C2) :**
* Florian BAEZA
* Liza HARCHEB

**Technologies :**
- Langage : Python
- Framework : Django
- Web : HTML + Bootstrap

**Gestion de projet et ressenti :**
- Nous conservons chaque branche sur le projet de sorte à illustrer l'historique des changements.
- Les commits sont squash dans chaque branche, mais peuvent donc être consultés sur leurs branches respectives.
- Notre ressenti sur ce projet est le suivant : nous avons apprécié réaliser ce projet et découvrir la technologie Django au travers de celui-ci et du cours, puisque c'était pour nous deux une totale découverte. La simplicité d'usage du langage et ses nombreux packages font que nous l'utiliserons sans doute à l'avenir et nous sommes contents de l'avoir découvert.

# Partie 0 : Initialisation du projet
**Branche :** /feature/init_project

**Commentaires :** aucun

# Partie 1 : Liste des sites
**Branche :** /feature/implement_sites_list

**Commentaires :**
- Nous avons inclus un attribut supplémentaire pour l'entité Site. Cet attribut est l'attribut "Description". Comme indiqué dans le code de l'entité (model), via un commentaire, cette description permet de définir à quoi sert ce combo identifiant/password. Un exemple y est donné :  si l'on possède plusieurs comptes sur un même site, il nous faut un moyen de les différencier. Cela peut être fait au travers de cette description.
- Nous avons fait le choix de laisser la base de données dans le projet, de sorte à ce qu'il embarque déjà des données prêtes à l'emploi. 
- L'utilisateur admin par défaut de notre projet possède les identifiants de connexion au backoffice suivants : 
	- Identifiant : root
	- Mot de passe : password

# Partie 2 : Ajout d'un site
**Branche :** /feature/site_creation_form

**Commentaire :** aucun

# Partie 3 : Modification d'un site
**Branche :** /feature/site_edition_form

**Commentaire :** aucun

# Partie 4 : Suppression d'un site
**Branch :** /feature/delete_site

**Commentaire :** aucun

# Partie 5 : Mise en place du design
**Branch :** /feature/implement_design

**Commentaires :**
- Utilisation du package pip "django-bootstrap-v5" (> pip install django-bootstrap-v5).
- Sous une certaine mesure, le site est Responsive. Il reste pensé principalement pour le mode PC, mais certains points ont été implémentés pour rendre l'expérience viable sur un support réduit. On peut penser à la table de la liste des sites, qui ne déborde pas mais va être scrollable horizontalement, au header qui s'adapte en termes de layout ainsi qu'au formulaire d'ajout/édition dont les champs vont occuper plus ou moins de place.